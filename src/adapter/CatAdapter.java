package adapter;

public class CatAdapter implements Dog {

	Cat cat;
	
	public CatAdapter(Cat cat) {
		this.cat = cat;
	}
	
	@Override
	public void bark() {
		cat.meow();
	}

}
