package adapter;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		Dog lab = new LabradorRetriever();
		Cat persian = new PersianCat();
		Dog persianCatDog = new CatAdapter(persian);
		ArrayList<Dog> dogs = new ArrayList<>();
		
		dogs.add(lab);
		dogs.add(persianCatDog);
//		Class c = persian.getClass();
//		Class d = lab.getClass();
		
		for(Dog dog1 : dogs) {
			System.out.println(dog1);
			dog1.bark();
		}

	}

}
